using Mephi.Cipher.KLEIN.Ciphers.Algorithms;
using Mephi.Cipher.KLEIN.Ciphers.Helpers;

namespace Mephi.Cipher.Tests;

[TestFixture]
public class KleinTests
{
    [Test]
    public void TestEncodingWithDiffKeys()
    {
        byte[] message = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

        var key1 = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        var key2 = new byte[] {0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
            
        byte[] expectedOne = {0xcd, 0xc0, 0xb5, 0x1f, 0x14, 0x72, 0x2b, 0xbe};
        byte[] expectedTwo = {0x59, 0x23, 0x56, 0xc4, 0x99, 0x71, 0x76, 0xc8};

        var klein = new Klein64(key1);

        var cipherTextOne = klein.Encrypt(message);
            
        klein = new Klein64(key2);

        var cipherTextTwo = klein.Encrypt(message);
        
        Assert.Multiple(() =>
        {
            Assert.That(cipherTextOne, Is.EqualTo(expectedOne), "Cipher texts are not equal, but should be!");
            Assert.That(cipherTextTwo, Is.EqualTo(expectedTwo), "Cipher texts are not equal, but should be!");
        });
    }
    
    [Test]
    public void TestEncodingWithOnlyZeroKey()
    {
        byte[] key1 = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
        byte[] key2 = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            
        byte[] message1 = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        byte[] expected1 = {0x64, 0x56, 0x76, 0x4e, 0x86, 0x02, 0xe1, 0x54};
            
        byte[] message2 = {0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef};
        byte[] expected2 = {0x62, 0x9f, 0x9d, 0x6d, 0xff, 0x95, 0x80, 0x0e};

        var klein = new Klein64(key1);

        var cipher1 = klein.Encrypt(message1);
            
        klein = new Klein64(key2);

        var cipher2 = klein.Encrypt(message2);
        Assert.Multiple(() =>
        {
            Assert.That(cipher1, Is.EqualTo(expected1), "Cipher texts are not equal, but should be!");
            Assert.That(cipher2, Is.EqualTo(expected2), "Cipher texts are not equal, but should be!");
        });
    }
    
    [Test]
    public void TestGetCustomDiffusionMatrix()
    {
        var matrix = KleinHelper.GetDiffusionMatrix(new byte[] {3, 1, 1, 2});
        
        Assert.That(new []
        {
           new byte [] { 2, 3, 1, 1 },
           new byte [] { 1, 2, 3, 1 },
           new byte [] { 1, 1, 2, 3 },
           new byte [] { 3, 1, 1, 2 }
            
        }, Is.EqualTo(matrix));
    }
}