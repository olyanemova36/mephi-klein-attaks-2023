namespace Mephi.Cipher.KLEIN.Ciphers.Enums
{
    public enum KleinConfigurations
    {
        Easy = 8, 
        Medium = 10, 
        Hard = 12
    }
}