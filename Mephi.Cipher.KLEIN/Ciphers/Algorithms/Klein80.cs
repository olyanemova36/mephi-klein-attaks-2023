using Mephi.Cipher.KLEIN.Ciphers.Enums;

namespace Mephi.Cipher.KLEIN.Ciphers.Algorithms;

public class Klein80 : Klein
{
    protected override int R => 16;
    protected override int BLOCK_SIZE_BITS => 80;
    
    protected Klein80(byte[] key) : base(KleinConfigurations.Medium, key)
    {
    }
}