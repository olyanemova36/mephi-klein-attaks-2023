using Mephi.Cipher.KLEIN.Ciphers.Enums;

namespace Mephi.Cipher.KLEIN.Ciphers.Algorithms;

public class Klein64 : Klein
{
    protected override int R => 12;
    protected override int BLOCK_SIZE_BITS => 64;

    public Klein64(byte[] key) : base(KleinConfigurations.Easy, key)
    {
        
    }
}