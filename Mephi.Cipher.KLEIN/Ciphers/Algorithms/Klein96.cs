using Mephi.Cipher.KLEIN.Ciphers.Enums;

namespace Mephi.Cipher.KLEIN.Ciphers.Algorithms;

public class Klein96 : Klein
{
    protected override int R => 20;
    protected override int BLOCK_SIZE_BITS => 96;
    
    protected Klein96(byte[] key) : base(KleinConfigurations.Hard, key)
    {
    }
}