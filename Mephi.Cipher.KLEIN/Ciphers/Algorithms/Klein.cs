using Mephi.Cipher.KLEIN.Ciphers.Enums;
using Mephi.Cipher.KLEIN.Ciphers.Helpers;
using Mephi.Cipher.KLEIN.Ciphers.Keys;
using Mephi.Cipher.KLEIN.Exceptions;
using Mephi.Cipher.KLEIN.Extensions;

namespace Mephi.Cipher.KLEIN.Ciphers.Algorithms;

public abstract class Klein
{
    protected virtual int R { get; }
    protected virtual int BLOCK_SIZE_BITS { get; }
    protected int BLOCK_SIZE_BYTE => BLOCK_SIZE_BITS / 8;
    protected byte[][] DIFFUSION_MATRIX => KleinHelper.GetDiffusionMatrix(new byte[] {3, 1, 1, 2});

    private IKeyProvider _keyProvider;
    
    private const int ROTATE_BYTE = 2;
    private const int HALF_BYTE = 16;
    
    protected Klein(KleinConfigurations configuration, byte[] key)
    {
        switch (configuration)
        {
            case KleinConfigurations.Easy:
                R = 12;
                BLOCK_SIZE_BITS = 64;
                break;
            case KleinConfigurations.Medium:
                R = 16;
                BLOCK_SIZE_BITS = 80;
                break;
            case KleinConfigurations.Hard:
                R = 20;
                BLOCK_SIZE_BITS = 96;
                break;
        }
        
        if (key.Length != BLOCK_SIZE_BITS / 8)
        {
            throw new InconsistentKleinParameterException($"Incorrect key size : \"{key.Length}\" B for configuration \"{configuration:g}\"\n\t" +
                                                          $"It supports only \"{BLOCK_SIZE_BYTE}\" B size.");
        }
        
        _keyProvider = new KleinKeySchedule(key);
    }
    
    public virtual byte[] Encrypt(byte[] plainText) 
    {
        if (IsMessageValid(plainText)) // only 64 bits => 2^3 -> byte => 8 bytes
        {
            var result = plainText;
            
            for (var r = 0; r < R; ++r)
            {
                result = MixNibbles(RotateNibbles(SubNibbles(AddRoundKey(result, r))));
            }
            
            return result.Xor(_keyProvider.GetRoundKey(R)); // Last Round
        }
        else
        {
            throw new ArgumentException("Can't transform not valid message for cipher");
        }
    }

    private byte[] SubNibbles(byte[] block)
    {
        var result = new byte[BLOCK_SIZE_BYTE];
            
        for (var i = 0; i < BLOCK_SIZE_BYTE; ++i)
        {
            result[i] = SBox(block[i]);
        }

        return result;
    }
    private byte[] RotateNibbles(byte[] block) // TWO BYTES LEFT
    {
        var result = new byte[BLOCK_SIZE_BYTE];
            
        for (var i = 0; i < BLOCK_SIZE_BYTE; ++i)
        {
            result[(i + (BLOCK_SIZE_BYTE - ROTATE_BYTE)) % BLOCK_SIZE_BYTE] = block[i];
        }

        return result;
    }

    public static byte SBox(byte @byte)
        => (byte) (SubNibblesHigh((byte) (@byte / HALF_BYTE * HALF_BYTE)) +
                   SubNibblesLower((byte) (@byte % HALF_BYTE)));
    
    private static byte SubNibblesHigh(byte @byte) => @byte switch
    {
        0x00 => 0x70,
        0x10 => 0x40,
        0x20 => 0xa0,
        0x30 => 0x90,
        0x40 => 0x10,
        0x50 => 0xf0,
        0x60 => 0xb0,
        0x70 => 0x00,
        0x80 => 0xc0,
        0x90 => 0x30,
        0xa0 => 0x20,
        0xb0 => 0x60,
        0xc0 => 0x80,
        0xd0 => 0xe0,
        0xe0 => 0xd0,
        0xf0 => 0x50,
        _ => throw new ArgumentOutOfRangeException(nameof(@byte), @byte, null)
    };
    
    private static byte SubNibblesLower(byte @byte) => @byte switch
    {
        0x00 => 0x07,
        0x01 => 0x04,
        0x02 => 0x0a,
        0x03 => 0x09,
        0x04 => 0x01,
        0x05 => 0x0f,
        0x06 => 0x0b,
        0x07 => 0x00,
        0x08 => 0x0c,
        0x09 => 0x03,
        0x0a => 0x02,
        0x0b => 0x06,
        0x0c => 0x08,
        0x0d => 0x0e,
        0x0e => 0x0d,
        0x0f => 0x05,
        _ => throw new ArgumentOutOfRangeException(nameof(@byte), @byte, null)
    };

    private byte[] MixNibbles(byte[] block)
    {
        var tupleSize = BLOCK_SIZE_BYTE / 2;

        var firstTuple = new byte[tupleSize];
        var secondTuple = new byte[tupleSize];

        for (int i = 0; i < tupleSize; i++)
        {
            var diffusionLine = DIFFUSION_MATRIX[i];

            firstTuple[i] = diffusionLine
                .Zip(block[..tupleSize], GMul)
                .Aggregate((x, y) => (byte) (x ^ y));
            
            secondTuple[i] = diffusionLine
                .Zip(block[tupleSize..], GMul)
                .Aggregate((x, y) => (byte) (x ^ y));
        }

        return firstTuple.Concat(secondTuple).ToArray();
    }

    private bool IsMessageValid(byte[] message) => message.Length == BLOCK_SIZE_BYTE;

    private byte[] AddRoundKey(byte[] plainText, int round) => plainText.Xor(_keyProvider.GetRoundKey(round));
    
    private static byte GMul(byte a, byte b) { // Galois Field (2^8) Multiplication of two Bytes
        byte p = 0;

        for (var counter = 0; counter < 8; counter++) {
            if ((b & 1) != 0) {
                p ^= a;
            }

            var hiBitSet = (a & 0x80) != 0;
            a <<= 1;
            if (hiBitSet) {
                a ^= 0x1B; /* x^8 + x^4 + x^3 + x + 1 */
            }
            b >>= 1;
        }

        return p;
    }
}