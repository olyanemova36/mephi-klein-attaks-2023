namespace Mephi.Cipher.KLEIN.Ciphers.Helpers;

public static class KleinHelper
{
    public static byte[][] GetDiffusionMatrix(byte[] polynomial)
    {
        var result = new byte[polynomial.Length][];
        for (var i = polynomial.Length - 1 ; i >= 0; --i)
        {
            if (i == polynomial.Length - 1 )
            {
                result[i] = polynomial;
            }
            else
            {
                result[i] = LeftRotate(result[i + 1]);
            }
        }
        
        return result;
    }

    private static byte[] LeftRotate(byte[] arr)
    {
        var result = new byte[arr.Length];
        
        for (var i = 0; i < arr.Length - 1; i++)
        {
            result[i] = arr[i + 1];
        }
        
        result[^1] = arr[0];

        return result;
    }
}