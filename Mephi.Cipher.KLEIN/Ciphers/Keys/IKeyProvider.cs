namespace Mephi.Cipher.KLEIN.Ciphers.Keys;

public interface IKeyProvider
{
    public byte[] GetRoundKey(int round);
}