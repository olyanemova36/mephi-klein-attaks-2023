using Mephi.Cipher.KLEIN.Ciphers.Algorithms;
using Mephi.Cipher.KLEIN.Extensions;

namespace Mephi.Cipher.KLEIN.Ciphers.Keys;

/// <summary>
/// KLEIN Key Schedule class. Supports key sizes as 64 / 80 / 96 bites. 
/// </summary>
public class KleinKeySchedule : IKeyProvider
{
    private byte[] _key;
    
    private int KEY_SIZE_BYTES => _key.Length;
    private int MIDDLE => KEY_SIZE_BYTES / 2;
    private int SECOND_BYTE_II_PART => MIDDLE + 1;
    private int THIRD_BYTE_II_PART => SECOND_BYTE_II_PART + 1;
    
    private const int THIRD_BYTE_I_PART = 2;
    
    public KleinKeySchedule(byte[] baseKey)
    {
        _key = baseKey;
    }

    public byte[] GetRoundKey(int round)
    {
        if (round != 0)
        {
            var firstPart = _key[..MIDDLE].Shift();
            var secondPart = _key[MIDDLE..].Shift();
            var xorResult = firstPart.Xor(secondPart);

            secondPart[THIRD_BYTE_I_PART] = (byte) (secondPart[THIRD_BYTE_I_PART] ^ round);

            xorResult[SECOND_BYTE_II_PART - MIDDLE] = Klein.SBox(xorResult[SECOND_BYTE_II_PART - MIDDLE]);
            xorResult[THIRD_BYTE_II_PART - MIDDLE] = Klein.SBox(xorResult[THIRD_BYTE_II_PART - MIDDLE]);

            _key = secondPart.Concat(xorResult).ToArray();
        }
        Console.WriteLine(_key.Length);
        return _key;
    }
}