namespace Mephi.Cipher.KLEIN.Extensions;

public static class BytesExtensions
{
    public static byte[] Shift(this byte[] array)
    {
        if (array == null) throw new NullReferenceException("Can't shift the null reference byte array");
        
        var copy = new byte[array.Length];
                
        copy[0] = array[1];
        copy[1] = array[2];
        copy[2] = array[3];
        copy[array.Length - 1] = array[0];

        return copy;
    }
    
    public static byte[] Xor(this byte[] first, byte[] second)
    {
        if (IsTermsValid(first, second))
        {
            var lenght = first.Length;
            var result = new byte[lenght];

            for (var i = 0; i < lenght; ++i)
            {
                result[i] = (byte) (first[i] ^ second[i]);
            }

            return result;
        }

        throw new NotSupportedException("The method can't support \"XOR\" with not valid bytes");
    }

    private static bool IsTermsValid(IReadOnlyCollection<byte> first, IReadOnlyCollection<byte> second) => first.Count == second.Count;
}