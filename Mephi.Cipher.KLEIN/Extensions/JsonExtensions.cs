using Newtonsoft.Json;

namespace Mephi.Cipher.KLEIN.Extensions;

public static class JsonExtensions
{
    public static string ToJson<T>(this T @object) where T  : class, new() => JsonConvert.SerializeObject(@object);
}