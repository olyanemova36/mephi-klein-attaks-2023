namespace Mephi.Cipher.KLEIN.Exceptions;

/// <summary>
/// Exception, what occur when parameters for KLEIN cipher is inconsistent. 
/// </summary>
public class InconsistentKleinParameterException : Exception
{
    public InconsistentKleinParameterException() { }

    public InconsistentKleinParameterException(string message)
        : base(message) { }

    public InconsistentKleinParameterException(string message, Exception inner)
        : base(message, inner) { }
}